import React from 'react';
import ImageCard from "../../components/ImageCard";
import {Card} from "react-bootstrap";

import Enzyme, {shallow} from "enzyme";
import ReactSeventeenAdapter from "@wojtekmaj/enzyme-adapter-react-17";
Enzyme.configure({ adapter: new ReactSeventeenAdapter() });

test('ImageCard - src only', () => {
    const imageSrc = "test.jpg"
    const imageCard = shallow(<ImageCard src={imageSrc} />);

    //src should be equal
    expect(imageCard.find(Card.Img).prop('src')).toEqual(imageSrc);
    //button should be hidden
    expect(imageCard.find(Card.Body).exists()).toBeFalsy()
});

test('ImageCard - with href', () => {
    const imageSrc = "test.jpg"
    const imageCard = shallow(<ImageCard src={imageSrc} href="/test"/>);

    //src should be equal
    expect(imageCard.find(Card.Img).prop('src')).toEqual(imageSrc);
    //button should be shown
    expect(imageCard.find(Card.Body).exists()).toBeTruthy()
});
