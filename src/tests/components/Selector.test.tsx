import React from 'react';
import Selector from "../../components/Selector";

import {Form} from "react-bootstrap";

import Enzyme, {shallow} from "enzyme";
import ReactSeventeenAdapter from "@wojtekmaj/enzyme-adapter-react-17";
Enzyme.configure({ adapter: new ReactSeventeenAdapter() });

test('Selector', () => {
    //get selector
    const selector = shallow(<Selector options={[]} />);
    //label should not exist if we do not pass label prop
    expect(selector.find(Form.Label).exists()).toBeFalsy()
});

test('Selector - with label', () => {
    //get selector
    const selector = shallow(<Selector label="breed" options={[]} />);
    //check if label exists
    expect(selector.find(Form.Label).exists()).toBeTruthy()
});

test('Selector - with options', () => {
    const options = [
        { id: 1, label: "test1", value: "value1"},
        { id: 2, label: "test2", value: "value2"},
        { id: 3, label: "test3", value: "value3"}
    ]
    //get selector component
    const selector = shallow(<Selector label="breed" options={options} />);
    //check if selector component has expected components
    selector.find(Form.Control).find('option').forEach( option => {
        expect(
            options
                .map(op => op.value)
                .find(value => value === option.prop('value'))
        ).toBeTruthy()
    })
});