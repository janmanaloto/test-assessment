import React from 'react';
import { render, screen } from '@testing-library/react';
import Page from "../../components/Page";
import {SITE_TITLE} from "../../config/main";

test('Page title - default', () => {
    render(<Page/>);
    const title = document.title;
    expect(title).toEqual(`${SITE_TITLE}`)
});

test('Page title - pass title prop', () => {
    render(<Page title="test"/>);
    const title = document.title;
    expect(title).toEqual(`${SITE_TITLE} - test`)
});

test('Page content', () => {
    render(<Page>Test Content</Page>);
    const content = screen.getByText(/Test Content/i);
    expect(content).toBeInTheDocument();
});
