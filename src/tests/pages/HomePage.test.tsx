import React from 'react';
import { render, screen } from '@testing-library/react';
import HomePage from "../../pages/HomePage";
import {SITE_TITLE} from "../../config/main";

test('Basic HomePage test', () => {
    render(<HomePage/>);
    const title = document.title;
    const content = screen.getByText(SITE_TITLE);
    expect(content).toBeInTheDocument();
    expect(title).toEqual(`${SITE_TITLE} - Home`);
});
