import {getBreedList} from "../../services/catService";


test('getCatBreedList', async () => {
    const breedList = await getBreedList();
    expect(breedList.data.length).toBeGreaterThan(0);
});

test('getCatBreedList - wrong url', async () => {
    //should throw error because of wrong url
    await expect(getBreedList( "test")).rejects.toThrow();
})

it('getCatBreedList - wrong api key', async () => {
    const breedList = await getBreedList(null, {"x-api-key": "test"});
    // API works even though x-api-key is wrong - this is confirmed when checking the request header of the response
    expect(breedList.data.length).toBeGreaterThan(0);
})