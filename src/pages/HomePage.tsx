import React, {useEffect, useState} from 'react';
import Page from "../components/Page";
import {QUERY_KEYS, SITE_TITLE} from "../config/main";
import Selector from "../components/Selector";
import useQuery from "../hooks/useQuery";
import {Row} from "react-bootstrap";
import {catActions} from "../actions";
import {useDispatch, useSelector} from "react-redux";
import {Breed} from "../types/Breed";
import ImageCardList from "../components/ImageCardList";
import {Button} from "react-bootstrap";

function HomePage(props: any) {
    const dispatch = useDispatch();
    const query = useQuery();
    const [breedId, setBreedId] = useState(query.get(QUERY_KEYS.breed) || undefined);
    const catState: any = useSelector(state => state);
    const {status, catListPage} = catState;
    const {isBreedListLoading, isCatListLoading, isCatListSuccess, isCatListMaxPage} = status || false;
    const [breedOptions, setBreedOptions] = useState<any>(catState.breedList);
    const [catList, setCatList] = useState([]);

    //initial load - fetch breed list
    useEffect(() => {
        dispatch(catActions.getBreedList());
    }, []);

    //breedlist loaded or changed - map fetched list to options
    useEffect(() => {
        const {breedList} = catState;
        const mappedBreedListToOptions = (breedList || []).map((breed: Breed) => ({ id: breed.id, label: breed.name, value: breed.id}))
        setBreedOptions(mappedBreedListToOptions);
    }, [catState.breedList])

    //cat list loaded or changed - map list to image cards
    useEffect(() => {
        const {catList} = catState;
        const mappedCatListToCard = catList.map((cat: any) => ({id: cat.id, src: cat.url}))
        setCatList(mappedCatListToCard);
    }, [catState.catList])

    //change url query if breed is changed
    useEffect(() => {
        // set cat list from API
        if(!!breedId) {
            props.history.push({search: `?breed=${breedId}`});
            dispatch(catActions.clearCatList());
            fetchCatList();
        }
    }, [breedId]);

    //handle select change
    const onSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setBreedId(event.target.value);
    }
    const fetchCatList = (page?: number) => {
        !!breedId && dispatch(catActions.getCatList(breedId, page));
    }

    console.log("MAX", isCatListMaxPage);
    return (
        <Page title="Home">
            <h1>{SITE_TITLE}</h1>
            <Selector
                label="Breed"
                selected={breedId}
                options={breedOptions}
                onChange={onSelectChange}
                placeholder={"Select Breed..."}
                loading={isBreedListLoading}
            />
            <Row md={4}>
                <ImageCardList
                    imageCards={catList}
                    loading={isCatListLoading} />
            </Row>
            {/*Only show Load More button if fetching of cat list is successful and current page is not on last page*/}
            {
                !!isCatListSuccess &&
                !isCatListMaxPage &&
                <Button
                    variant="success"
                    disabled={isCatListLoading}
                    onClick={() => fetchCatList(catListPage+1)}>
                    Load More
                </Button>
            }
        </Page>
    );
}

export default HomePage;
