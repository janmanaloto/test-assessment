import React, {PropsWithChildren, useEffect} from "react";
import ImageCard from "../components/ImageCard";
import Page from "../components/Page";
import { useParams } from "react-router-dom";
import {IParamTypes} from "../interfaces/IParamTypes";
import {useDispatch, useSelector} from "react-redux";
import {catActions} from "../actions";
import {Cat} from "../types/Cat";
import CatDetails from "../components/CatDetails";
import {Navbar, Button} from "react-bootstrap";

function CatPage(props: PropsWithChildren<any>) {
    const { catId } = useParams<IParamTypes>();
    const dispatch = useDispatch();
    const catState: any = useSelector(state => state);
    const { isCatLoading } = catState.status;
    const cat: Cat = catState.cat;

    useEffect(() => {
        dispatch(catActions.getCatById(catId))
    }, []);

    return (
        <Page>
            <Navbar>
                <Button onClick={props.history.goBack}>Back</Button>
            </Navbar>
            <ImageCard id={props.id} loading={isCatLoading} src={!!cat && cat.image}>
                <CatDetails loading={isCatLoading} cat={cat} />
            </ImageCard>
        </Page>
    )
}

export default CatPage;