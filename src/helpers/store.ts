import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import {catReducer} from "../reducers/catReducer";

export const store = createStore(
    catReducer,
    applyMiddleware(
        thunkMiddleware
    )
);