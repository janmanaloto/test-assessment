export const SITE_TITLE = "Cat Browser";
export const QUERY_KEYS = {
    breed: "breed"
};

export const CAT_API_URL = "https://api.thecatapi.com/v1";
export const CAT_API_KEY = "d39e8ea1-8ce7-4d52-8596-0584c6a50c08";

export const PAGING = {
    defaultPage: 1,
    defaultSize: 10,
}

export const ERROR_MESSAGE = "Apologies but we could not load new cats for you at this time! Miau!";