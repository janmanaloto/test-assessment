import {SET_BREED_LIST_EVENTS} from "../events/breedListEvents";
import {Action} from "../types/Action";
import {SET_CAT_LIST_EVENTS} from "../events/catListEvents";
import {SET_CAT_EVENTS} from "../events/catEvents";
import {SiteState} from "../types/SiteState";
import {Cat} from "../types/Cat";

export const initialState: SiteState = {
    status: {},
    breedList: [],
    catList: [],
    hasError: false
}

export const catReducer = (state: any = initialState, action: Action) => {
    const {type, payload} = action || {};

    switch (type) {
        case SET_BREED_LIST_EVENTS.LOADING:
            return {...state, status: {...state.status, isBreedListLoading: true}};
        case SET_BREED_LIST_EVENTS.FAILED:
            return {...state, status: {...state.status, isBreedListLoading: false, isBreedListSuccess: false}, hasError: true};
        case SET_BREED_LIST_EVENTS.SUCCESS:
            return {...state, status: {...state.status, isBreedListLoading: false, isBreedListSuccess: true}, breedList: payload}
        case SET_CAT_LIST_EVENTS.LOADING:
            return {...state, status: {...state.status, isCatListLoading: true}};
        case SET_CAT_LIST_EVENTS.FAILED:
            return {...state, status: {...state.status, isCatListLoading: false, isCatListSuccess: false}, hasError: true};
        case SET_CAT_LIST_EVENTS.SUCCESS:
            const {catList, page, isMaxPage} = payload;
            const existingList = state.catList;
            const finalCatList = page > 1 ? [...existingList, ...catList] : catList;
            return {...state, status: {...state.status, isCatListLoading: false, isCatListSuccess: true, isCatListMaxPage: isMaxPage}, catList: finalCatList, catListPage: page}
        case SET_CAT_EVENTS.LOADING:
            return {...state, status: {...state.status, isCatLoading: true}};
        case SET_CAT_EVENTS.FAILED:
            return {...state, status: {...state.status, isCatLoading: false, isCatSuccess: false}, hasError: true};
        case SET_CAT_EVENTS.SUCCESS:
            const { name: breed, origin, temperament, description } = payload.breeds[0];
            const cat: Cat = {
                image: payload.url,
                id: payload.id,
                breed,
                origin,
                temperament,
                description
            }
            console.log(cat);
            return {...state, status: {...state.status, isCatLoading: false, isCatSuccess: true}, cat}
        default:
            return state
    }
}