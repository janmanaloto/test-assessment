import {Cat} from "./Cat";

export type SiteState = {
    status: {},
    breedList: [],
    catList: [],
    cat?: Cat,
    hasError: boolean
}