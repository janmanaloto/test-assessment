export type Breed = {
    id: string | number,
    name: string,
    temperament: string,
    life_span: string,
    alt_names: string
}