export type Action = {
    type: string,
    payload: any
};

export interface IAction {
    type: string,
    payload: any
}