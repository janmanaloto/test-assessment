export type ImageCard = {
    id: string,
    src: string
}