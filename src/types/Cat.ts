export type Cat = {
    id: string,
    image: string,
    breed: string,
    origin: string,
    temperament: string,
    description: string
}