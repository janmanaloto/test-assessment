import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import CatPage from "./pages/CatPage";

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/cat/:catId" component={CatPage}/>
                <Route path="/" component={HomePage}/>
            </Switch>
        </Router>
    );
}

export default App;
