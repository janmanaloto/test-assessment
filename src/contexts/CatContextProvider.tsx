import React, {PropsWithChildren, ReactChildren} from "react";
import {createContext, useReducer} from "react";
import {catReducer, initialState} from "../reducers/catReducer";

const CatContext = createContext<any>({});

const CatProvider = ({children}: PropsWithChildren<{}> ) => {
    const [state, dispatch] = useReducer(catReducer, initialState);
    const value = [state, dispatch];
    return (
        <CatContext.Provider value={value}>{children}</CatContext.Provider>
    )
}

export {CatContext, CatProvider};