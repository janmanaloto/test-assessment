export const SET_CAT_LIST_EVENTS = {
    LOADING: "SET_CAT_LIST_LOADING",
    FAILED: "SET_CAT_LIST_FAILED",
    SUCCESS: "SET_CAT_LIST_SUCCESS"
}