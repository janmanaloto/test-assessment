import {CAT_API_KEY, CAT_API_URL, PAGING} from "../config/main";

const defaultHeader = {
    "content-type":"application/json",
    "accept":"application/json",
    "x-api-key": CAT_API_KEY
}

async function getBreedList(
    url?: string | null,
    header?: object
) {
    const input = url || `${CAT_API_URL}/breeds`;
    const init = {headers: {...defaultHeader, ...header} };
    return await fetch(input, init);
}

async function getCatList(breedId: string, page: number = PAGING.defaultPage) {
    const input = `${CAT_API_URL}/images/search?page=${page}&limit=${PAGING.defaultSize}&breed_id=${breedId}`;
    const init = {headers: defaultHeader }
    return fetch(input, init)
}

async function getCatById(catId: string) {
    const input = `${CAT_API_URL}/images/${catId}`;
    const init = {headers: defaultHeader }
    return fetch(input, init)
}

export const CatService = {
    getBreedList,
    getCatList,
    getCatById
}