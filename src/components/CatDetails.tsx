import React, {FunctionComponent} from "react";
import {Cat} from "../types/Cat";
import Skeleton from "react-loading-skeleton";

interface CatDetailsProps {
    loading: boolean,
    cat: Cat
}

const CatDetails: FunctionComponent<CatDetailsProps> = ({cat= {}, loading= false}) => {
    if (loading) {
        return (
            <>
                <h1><Skeleton/></h1>
                <h1><Skeleton/></h1>
                <h1><Skeleton/></h1>
            </>
        )
    } else {
        return (
            <>
                <h1>{cat.breed}</h1>
                <h3>Origin: {cat.origin}</h3>
                <h5>{cat.temperament}</h5>
                <p>{cat.description}</p>
            </>
        )
    }
}

export default CatDetails;