import React, {FunctionComponent, useEffect} from "react";
import {Container, Alert} from "react-bootstrap";

import {ERROR_MESSAGE, SITE_TITLE} from "../config/main";
import {useSelector} from "react-redux";

import '../styles/components/Page.css';

export interface PageProps {
    title?: string
}

const Page: FunctionComponent<PageProps> = props => {
    // check if title is passed so we can append it to SITE_TITLE
    const title = !!props.title ? `${SITE_TITLE} - ${props.title}` : SITE_TITLE;
    const {hasError}: any = useSelector(state => state);

    //document title can be changed everytime we update title prop
    useEffect(() => {
        document.title = title
    }, [title])

    //wrap children in Container or shared page components
    return (
        <>
            {hasError && <Alert variant="danger" className="fixed-top">{ERROR_MESSAGE}</Alert>}
            <Container>
                {props.children}
            </Container>
        </>
    )
}

export default Page;