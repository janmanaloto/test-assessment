import React, {FunctionComponent} from 'react'
import '../styles/components/CardContainer.css';

interface CardContainerProps {
    children?: any
}

const CardContainer: FunctionComponent<CardContainerProps> = ({children}) => {
    return (
        <div className="card-container">
            {children}
        </div>
    )
}

export default CardContainer;