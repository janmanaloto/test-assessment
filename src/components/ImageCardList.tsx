import React, {FunctionComponent} from 'react'
import ImageCard from "./ImageCard";
import CardContainer from "./CardContainer";

interface ImageCardListProps {
    imageCards: any,
    loading: boolean
}
const ImageCardList: FunctionComponent<ImageCardListProps> = ({imageCards, loading}) => {
    //@ts-ignore
    const imageLoader = Array(3).fill().map(key => (
        <CardContainer>
            <ImageCard id={key} loading={true}/>
        </CardContainer>
    ));
    const cards = imageCards && imageCards.map(({id, src}: any) => (
        <CardContainer>
            <ImageCard id={id} src={src} href={`/cat/${id}`}/>
        </CardContainer>
    ));

    return (
        <>
            {cards}
            {loading && imageLoader}
        </>
    )
}

export default ImageCardList;