import React, {FunctionComponent} from 'react'
import {Button, Card} from "react-bootstrap";
import {Link} from "react-router-dom";
import Skeleton from "react-loading-skeleton";
import '../styles/components/ImageCard.css';

interface ImageCardProps {
    id: string | number,
    src?: string,
    href?: string,
    loading?: boolean,
    children?: any
}

const ImageCard: FunctionComponent<ImageCardProps> = ({id, src, href, loading, children}) => {
    const linkEl = !!href && <Link to={href || ""}><Button className="btn-block">View Details</Button></Link>;
    return (
        <Card className="image-card">
            {loading ? <Skeleton height={250}/> : <Card.Img key={id} src={src} variant="top" className="image-card-img"/> }
            <Card.Body>
                {loading ? <Skeleton/> : linkEl }
                {children}
            </Card.Body>
        </Card>
    )
}

export default ImageCard;