import React, {ChangeEvent, FunctionComponent} from "react";
import {Form} from "react-bootstrap";
import Skeleton from "react-loading-skeleton";

export interface Option {
    id: string | number,
    label: string,
    value: string
}

interface SelectorProps {
    label?: string,
    options: Option[],
    selected?: string | number,
    onChange?: any,
    placeholder?: string,
    loading: boolean
}

const Selector: FunctionComponent<SelectorProps> = ({
    label,
    options,
    selected,
    onChange,
    placeholder,
    loading
}) => {
    const [value, setValue] = React.useState(selected);

    const onChangeHandler = (event: ChangeEvent<HTMLSelectElement>) => {
        setValue(event.currentTarget.value);
        onChange(event);
    }
    if (loading) {
        return (<Skeleton/>);
    } else {
        return (
            <Form>
                <Form.Group>
                    {/*Check if we have label, otherwise do not use Form.label*/}
                    {!!label ? (<Form.Label>{label}</Form.Label>) : "" }
                    <Form.Control
                        as="select"
                        onChange={onChangeHandler}
                        value={value}
                    >
                        {!value && <option defaultChecked>{placeholder}</option>}
                        { options.map( option =>
                            <option
                                key={option.id}
                                value={option.value}
                            >{option.label}</option>
                        )}
                    </Form.Control>
                </Form.Group>
            </Form>
        )
    }
}

export default Selector;