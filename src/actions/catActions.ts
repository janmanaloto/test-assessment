import {SET_BREED_LIST_EVENTS} from "../events/breedListEvents";
import {CatService} from "../services/catService";
import {SET_CAT_LIST_EVENTS} from "../events/catListEvents";
import {PAGING} from "../config/main";
import {SET_CAT_EVENTS} from "../events/catEvents";

export const catActions = {
    getBreedList,
    getCatList,
    clearCatList,
    getCatById
}

function getBreedList() {
    return async (dispatch: any, getState: any) => {
        try {
            //set context status to loading
            dispatch({ type: SET_BREED_LIST_EVENTS.LOADING });
            //fetch breed list
            const response = await CatService.getBreedList();
            const breedList = await response.json();
            //set context status to success and add breed list to context
            dispatch({ type: SET_BREED_LIST_EVENTS.SUCCESS, payload: breedList },);
        } catch (error) {
            //set status to failed if we encounter an error
            dispatch({ type: SET_BREED_LIST_EVENTS.FAILED });
        }
    }
}

function getCatList(breedId: string, page: number = PAGING.defaultPage) {
    return async (dispatch: any, getState: any) => {
        try {
            //set context status to loading
            dispatch({ type: SET_CAT_LIST_EVENTS.LOADING });
            //fetch breed list
            const response = await CatService.getCatList(breedId, page);
            const headers =  await response.headers;
            const catList = await response.json();
            const totalCats = parseInt(headers.get("Pagination-Count") || "0");
            const maxPage = Math.ceil(totalCats / PAGING.defaultSize);
            const isMaxPage = page >= maxPage;
            //set context status to success and add breed list to context
            dispatch({ type: SET_CAT_LIST_EVENTS.SUCCESS, payload: {catList, page, isMaxPage} });
        } catch (error) {
            //set status to failed if we encounter an error
            dispatch({ type: SET_CAT_LIST_EVENTS.FAILED });
        }
    }
}

function getCatById(catId: string) {
    return async (dispatch: any) => {
        try {
            //set context status to loading
            dispatch({type: SET_CAT_EVENTS.LOADING});
            //fetch cat
            const response = await CatService.getCatById(catId);
            const cat = await response.json();
            //set context status to success and add breed list to context
            dispatch({type: SET_CAT_EVENTS.SUCCESS, payload: cat});
        } catch (error) {
            //set status to failed if we encounter an error
            dispatch({ type: SET_CAT_EVENTS.FAILED });
        }
    }
}

function clearCatList() {
    return async (dispatch: any) => {
        dispatch({type: SET_CAT_LIST_EVENTS.SUCCESS, payload: {catList: []}})
    }
}